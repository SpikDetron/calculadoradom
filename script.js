var botao=document.querySelector(".botao")
var botaoCalc=document.querySelector(".calcButton")
var total=0
var soma=0
let input=document.querySelector(".nota")


function pegaNota(){
    let nota=input.value
    nota=parseFloat(nota)
    var notaValida = !((!(nota>=0)&&!(nota<=0))||(nota>10)||nota<0)
    if(!notaValida){
        alert('Apenas números de 1 a 10')
        input.value=""

    } 
    if(notaValida){
        input.value=""
        return nota
    }
}

function printLinha(){
    var notas=document.querySelector(".notas")
    let nota=pegaNota()
    if(nota!=undefined){
        total+=1
        soma+=nota
        notas.innerHTML=notas.innerHTML+'A nota '+total+' é '+nota+'\n'
    }
}

function printMedia(){
    let mediaTexto = document.querySelector('h2')
    mediaTexto.innerHTML='A média é: '+soma/total
    soma=0
    total=0
    var notas=document.querySelector(".notas")
    notas.innerHTML=""
}


botaoCalc.addEventListener("click",printMedia)
botao.addEventListener("click",printLinha)